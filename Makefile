ASFLAGS += -I /usr/share/cc65/asminc
CFLAGS += -O -Os -Oi -T -Or

CC = cc65
AS = ca65
LD = ld65

ASSEMBLE = $(AS) $(ASFLAGS)
COMPILE = $(CC) $(CFLAGS)
LINK = $(LD) $(LDFLAGS)

ASM_SOURCES = evy.asm
C_SOURCES = playing.c render.c nmi.c stage.c
S_SOURCES = $(C_SOURCES:.c=.s)
OBJECTS =  $(ASM_SOURCES:.asm=.o) $(S_SOURCES:.s=.o)

.PHONY: all clean

all: evy.nes
clean:
	-rm -v $(OBJECTS) evy.nes $(S_SOURCES) sprites.h sprites.s sprites.chr

evy.nes: nes.cfg $(OBJECTS)
	$(LINK) -o$@ -C$^ /usr/share/cc65/lib/nes.lib

evy.o : evy.asm sprites.chr memcpy.asm sprites.chr test.stage.bin
	$(ASSEMBLE) -o$@ $<

playing.o : playing.s
	$(ASSEMBLE) -o$@ $<

playing.s : playing.c buttons.h movement.h
	$(COMPILE) -o$@ $<

render.o : render.s
	$(ASSEMBLE) -o$@ $<

render.s : render.c registers.h sprites.h movement.h
	$(COMPILE) -o$@ $<

nmi.o : nmi.s
	$(ASSEMBLE) -o$@ $<

nmi.s : nmi.c
	$(COMPILE) -o$@ $<

stage.o : stage.s
	$(ASSEMBLE) -o$@ $<

stage.s : stage.c
	$(COMPILE) -o$@ $<

sprites.chr sprites.h sprites.s : sprites.yaml sprites/player.png
	spritesheetc -i$< -osprites.chr -csprites.h -asprites.s

test.stage.bin: test.stage
	stagec -i$< -o$@
