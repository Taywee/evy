; vim: ft=asm_ca65
;
.segment "CHARS"
    .incbin "sprites.chr"
.segment "HEADER"
    .byte "NES",$1A
    ; PRG ROM, 32KB
    .byte 2
    ; CHR ROM, 8 KB
    .byte 1
    ; Flags 6
    .byte %00000001
    ; Flags 7
    .byte %00000000
    ; PRG RAM
    .byte $01
.segment "VECTORS"
    .word nmi, reset, 0

; all includes
.include "sprites.s"
.include "memcpy.asm"

.segment "RODATA"
palette:
    .byte $1D, $37, $3d, $18
    .byte $1D, $37, $3d, $18
    .byte $1D, $37, $3d, $18
    .byte $1D, $37, $3d, $18
    .byte $1D, $37, $3d, $18
    .byte $1D, $37, $3d, $18
    .byte $1D, $37, $3d, $18
    .byte $1D, $37, $3d, $18
end_palette:
s_palette = (end_palette - palette)

_stage:
    .incbin "test.stage.bin"

.segment "OAM"
_evy_sprites:
    .byte $80, LEFT_PLAYER_0_0, $00, $80
    .byte $80, LEFT_PLAYER_0_1, $00, $88
    .byte $88, LEFT_PLAYER_0_2, $00, $80
    .byte $88, LEFT_PLAYER_0_3, $00, $88

.ZEROPAGE
_buttons1:
    .res 2  ; player 1 gamepad buttons, one bit per button.  Second position is the previous frame's buttons.

.segment "INITZP": zeropage
STATETITLE     = $00  ; displaying title screen
STATEPLAYING   = $01  ; move paddles/ball, check for collisions
STATEGAMEOVER  = $02  ; displaying game over screen

nmiran:
    .byte $00
gamestate:
    .byte STATEPLAYING
_playerpos:
    .byte $80, $80
; framecount in this tenth-second, from 0-6
_framecount: 
    .byte $00
_totalframecount: 
    .byte $00
_seconds: 
    .byte $00, $00
_second_tenths: 
    .byte $00
_movementstate: 
    .byte $00
_i: 
    .byte $00

.segment "BSS"
; tiles used for metatiles.  16 tiles of 5 bytes a piece for up to 80 bytes
; total
_metatiles:
    .res $50
  
.segment "CODE"

.import PPUCTRL, PPUMASK, PPUSTATUS, OAMADDR, OAMDATA, PPUSCROLL, PPUADDR, PPUDATA, OAMDMA, CONTROLLER1, CONTROLLER2
.import __OAM_SIZE__, __OAM_RUN__, __OAM_LOAD__
.import __INITZP_SIZE__, __INITZP_RUN__, __INITZP_LOAD__
.import __DATA_SIZE__, __DATA_RUN__, __DATA_LOAD__

.exportzp _buttons1, _playerpos, _framecount, _totalframecount, _seconds, _second_tenths, _movementstate, _i
.export _evy_sprites, _stage, _metatiles

.import _EnginePlaying, _UpdateSprites, _Nmi, _SetupStage

vblankwait:
    bit PPUSTATUS
    bpl vblankwait
    rts

reset:
    sei          ; disable IRQs
    cld          ; disable decimal mode
    ldx #$40
    stx $4017    ; disable APU frame IRQ
    ldx #$FF
    txs          ; Set up stack
    inx          ; now X = 0
    jsr vblankwait
    stx $4010    ; disable DMC IRQs


    ldx #$0
    lda #$00
clrmem:
    sta $0000, x
    sta $0100, x
    sta $0200, x
    sta $0300, x
    sta $0400, x
    sta $0500, x
    sta $0600, x
    sta $0700, x
    inx
    bne clrmem

    ; Store sprites as FF, so they are off the bottom of the screen
    lda #$ff
@oamloop:
    sta __OAM_RUN__, x
    inx
    bne @oamloop

   
    jsr vblankwait

    lda #$00
    sta PPUCTRL    ; disable NMI
    sta PPUMASK    ; disable rendering

loadpalettes:
    lda PPUSTATUS             ; read PPU status to reset the high/low latch
    lda #$3f
    sta PPUADDR             ; write the high byte of $3F00 address
    lda #$00
    sta PPUADDR             ; write the low byte of $3F00 address
    ldx #$00              ; start out at 0
@loop:
    lda palette, x        ; load data from address (palette + the value in x)
                            ; 1st time through loop it will load palette+0
                            ; 2nd time through loop it will load palette+1
                            ; 3rd time through loop it will load palette+2
                            ; etc
    sta PPUDATA             ; write to PPU
    inx                   ; X = X + 1
    cpx #s_palette              ; compare x to hex $10, decimal 16 - copying 16 bytes = 4 sprites
    bne @loop  ; Branch to LoadPalettesLoop if compare was Not Equal to zero
                          ; if compare was equal to 32, keep going down
                          
; Load in the defined sections using memcpy
loaddefined:
    ldy #>__OAM_SIZE__
    ldx #<__OAM_SIZE__
    memcpy __OAM_RUN__, __OAM_LOAD__

    ldy #>__INITZP_SIZE__
    ldx #<__INITZP_SIZE__
    memcpy __INITZP_RUN__, __INITZP_LOAD__

    ldy #>__DATA_SIZE__
    ldx #<__DATA_SIZE__
    memcpy __DATA_RUN__, __DATA_LOAD__

; Loop through, and load the current level
setupstage:
    jsr _SetupStage

setupppu:
    lda #%10010000   ; enable NMI, sprites from Pattern Table 0, background from Pattern Table 1
    sta PPUCTRL

    lda #%00011110   ; enable sprites, enable background, no clipping on left side
    sta PPUMASK

forever:
    lda nmiran
    beq forever     ;jump back to Forever, infinite loop, waiting for NMI
    lda #0
    sta nmiran

    jsr ReadController1

GameEngine:  
    lda gamestate
    cmp #STATEPLAYING
    bne @done
    jsr _EnginePlaying   ;;game is playing
@done:  
  
    jsr _UpdateSprites
    jmp forever

nmi:
    ; push processor status, A, X, and Y
    php
    pha
    txa
    pha
    tya
    pha

    lda #$00
    sta OAMADDR       ; set the low byte (00) of the RAM address
    lda #>__OAM_RUN__
    sta OAMDMA       ; set the high byte (02) of the RAM address, start the transfer

    ;;this is the PPU clean up section, so rendering the next frame starts properly.
    lda #%10010000   ; enable NMI, sprites from Pattern Table 0, background from Pattern Table 1
    sta PPUCTRL
    lda #%00011110   ; enable sprites, enable background, no clipping on left side
    sta PPUMASK
    bit PPUSTATUS
    lda #0
    sta PPUSCROLL
    sta PPUSCROLL

    ; set status variable and increment timing variables
    lda #$01
    sta nmiran

    jsr _Nmi

    ; pop Y, X, A, and Proocessor Status
    pla
    tay
    pla
    tax
    pla
    plp
    rti
 
; Reads the controller addr, and stores the buttons in `buttons`.  First, this
; pushes the previous button setup into the second byte of buttons
.macro ReadController addr, buttons
    lda buttons
    sta buttons+1
    lda #$01
    sta CONTROLLER1
    sta buttons
    lsr A
    sta CONTROLLER1
@loop:
    lda addr
    lsr A            ; bit0 -> Carry
    rol buttons     ; bit0 <- Carry
    bcc @loop
.endmacro

ReadController1:
    ReadController CONTROLLER1, _buttons1
    rts
