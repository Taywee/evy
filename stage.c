/* Copyright © 2019 Taylor C. Richberger <taywee@gmx.com>
 * This code is released under the license described in the LICENSE file
 */

#include "registers.h"
#include <string.h>

struct Metatile {
    unsigned char palette;
    unsigned char tile[4];
};

extern struct Metatile metatiles[16];
extern unsigned char stage[];
extern unsigned char i;
#pragma zpsym ("i");

static unsigned char row;
static unsigned char column;

static unsigned char tileCount;
static unsigned char tile;

static unsigned char metatileCount;
static unsigned char bodyByteCount;
static unsigned int bodyStartingPoint;
static unsigned int bodyCurrentPos;
static unsigned char cachedTiles[15];

extern void SetupStage(void) {
    // Get ready to insert palette.  Set to sequential insert
    *PPUCTRL = 0x00;

    // Read ppustatus to reset address latch
    i = *PPUSTATUS;
    // Set PPU ram address;
    *PPUADDR = 0x3F;
    *PPUADDR = 0x00;

    // Write palette
    for (i = 1; i < 0x21; ++i) {
        *PPUDATA = stage[i];
    }

    metatileCount = stage[0x21];

    // Set up metatiles.  For some reason, cc65's memcpy doesn't work correctly
    // with a variable.
    for (i = metatileCount * sizeof(struct Metatile); i > 0; --i) {
        ((char *)metatiles)[i] = (stage + 0x22)[i];
    }

    bodyByteCount = stage[0x22 + sizeof(struct Metatile) * metatileCount];
    bodyStartingPoint = 0x23 + sizeof(struct Metatile) * metatileCount;

    // Set up orientation
    if (stage[0] & 0xF0) {
        *PPUCTRL = 0x14;
        // Horizontal
    } else {
        *PPUCTRL = 0x10;
        // Vertical
    }

    // Set PPU ram to beginning of nametable;
    i = *PPUSTATUS;
    *PPUADDR = 0x20;
    *PPUADDR = 0x00;

    row = 0;
    column = 0;
    // Blit out the stage, consuming all bytes
    // TODO: make this more flexible.  The stage should set up its initial state
    // based on the player entry position, and be prepared to scroll itself left
    // or right (or up or down)
    for (bodyCurrentPos = bodyStartingPoint; bodyCurrentPos - bodyStartingPoint < bodyByteCount; ++bodyCurrentPos) {
        i = stage[bodyCurrentPos];
        tile = i & 0x0F;
        // Blit out columns one at a time, caching read columns so that the odd
        // columns can be filled after the fact
        for (tileCount = ((i >> 4) & 0x0F) + 1; tileCount > 0; --tileCount) {
            cachedTiles[row] = tile;
            *PPUDATA = metatiles[tile].tile[0];
            *PPUDATA = metatiles[tile].tile[1];
            ++row;
            if (row >= 15) {
                // Fill next row
                i = *PPUSTATUS;
                *PPUADDR = 0x20;
                *PPUADDR = column * 2 + 1;
                for (i = 0; i < 15; ++i) {
                    *PPUDATA = metatiles[cachedTiles[i]].tile[2];
                    *PPUDATA = metatiles[cachedTiles[i]].tile[3];
                }
                row = 0;
                ++column;
                i = *PPUSTATUS;
                *PPUADDR = 0x20;
                *PPUADDR = column * 2;
            }
        }
    }
}
