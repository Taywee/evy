#include "buttons.h"
#include "movement.h"

extern unsigned char buttons1[2];
#pragma zpsym ("buttons1");
extern unsigned char playerpos[2];
#pragma zpsym ("playerpos");
extern unsigned char second_tenths;
#pragma zpsym ("second_tenths");

static unsigned char last_second_tenth = 0;
static signed char fallspeed = 1;

#define GROUND 0xC0

extern void EnginePlaying(void) {
    // Move
    if (BUTTON_UP_PRESSED(buttons1)) {
        if (movementstate & MOVEMENT_STANDING) {
            fallspeed = -4;
            movementstate &= ~MOVEMENT_STANDING;
        }
    }
    if (BUTTON_LEFT_DOWN(buttons1)) {
        --playerpos[0];
        movementstate |= MOVEMENT_RUNNING;
        movementstate |= MOVEMENT_LEFT;
    } else if (BUTTON_RIGHT_DOWN(buttons1)) {
        ++playerpos[0];
        movementstate |= MOVEMENT_RUNNING;
        movementstate &= ~MOVEMENT_LEFT;
    } else {
        movementstate &= ~MOVEMENT_RUNNING;
    }
    playerpos[1] += fallspeed;

    // Do this after to prevent sprite clipping into ground
    if (playerpos[1] > (GROUND - 8)) {
        playerpos[1] = GROUND - 8;
        movementstate |= MOVEMENT_STANDING;
        fallspeed = 1;
    } else {
        movementstate &= ~MOVEMENT_STANDING;
    }

    // Tick every tenth of a second
    if (second_tenths != last_second_tenth) {
        last_second_tenth = second_tenths;
        if (!(movementstate & MOVEMENT_STANDING) && fallspeed < 4) {
            ++fallspeed;
        }
    }
}
