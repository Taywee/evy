#ifndef REGISTERS_H
#define REGISTERS_H

#define PPUCTRL ((char*)0x2000)
#define PPUMASK ((char*)0x2001)
#define PPUSTATUS ((char*)0x2002)
#define OAMADDR ((char*)0x2003)
#define OAMDATA ((char*)0x2004)
#define PPUSCROLL ((char*)0x2005)
#define PPUADDR ((char*)0x2006)
#define PPUDATA ((char*)0x2007)
#define OAMDMA ((char*)0x4014)
#define CONTROLLER1 ((char*)0x4016)
#define CONTROLLER2 ((char*)0x4017)

#endif /* ifndef REGISTERS_H */
