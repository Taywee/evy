* Stick with the existing style
* Create an issue before creating a pull request.
    Every pull request should close an issue, even minor ones.