#ifndef BUTTONS_H
#define BUTTONS_H

// These are named this way because BUTTON_ is just a prefix, and we need a way
// to differentiate between the DOWN button and the DOWN action.
#define BUTTON_A_BUTTON 0x80
#define BUTTON_B_BUTTON 0x40
#define BUTTON_SELECT_BUTTON 0x20
#define BUTTON_START_BUTTON 0x10
#define BUTTON_UP_BUTTON 0x08
#define BUTTON_DOWN_BUTTON 0x04
#define BUTTON_LEFT_BUTTON 0x02
#define BUTTON_RIGHT_BUTTON 0x01

// Whether the button is down, buttons is a byte
#define BUTTON_DOWN(buttons, button) ((buttons & button) != 0)

// Whether the button is up, buttons is a byte
#define BUTTON_UP(buttons, button) ((buttons & button) == 0)

// Whether the button was down for this frame, but not the previous.  Buttons is
// a byte array
#define BUTTON_PRESSED(buttons, button) (BUTTON_DOWN(buttons[0], button) && BUTTON_UP(buttons[1], button))

// Whether the button was down for this frame and the previous
#define BUTTON_HELD(buttons, button) (BUTTON_DOWN(buttons[0], button) && BUTTON_DOWN(buttons[1], button))

// Whether the button was down for the previous frame, but not this one
#define BUTTON_RELEASED(buttons, button) (BUTTON_UP(buttons[0], button) && BUTTON_DOWN(buttons[1], button))

// These are all the macros for use.  Every one of them takes the buttons array
// and accesses it as needed.  If you need to, for instance, get the specific
// state of the previous frame, you'll need to call the base DOWN or UP macro
// manually or do the check yourself, but that's really not hard.

// PRESSED
#define BUTTON_A_PRESSED(buttons) BUTTON_PRESSED(buttons, BUTTON_A_BUTTON)
#define BUTTON_B_PRESSED(buttons) BUTTON_PRESSED(buttons, BUTTON_B_BUTTON)
#define BUTTON_START_PRESSED(buttons) BUTTON_PRESSED(buttons, BUTTON_START_BUTTON)
#define BUTTON_SELECT_PRESSED(buttons) BUTTON_PRESSED(buttons, BUTTON_SELECT_BUTTON)
#define BUTTON_UP_PRESSED(buttons) BUTTON_PRESSED(buttons, BUTTON_UP_BUTTON)
#define BUTTON_DOWN_PRESSED(buttons) BUTTON_PRESSED(buttons, BUTTON_DOWN_BUTTON)
#define BUTTON_LEFT_PRESSED(buttons) BUTTON_PRESSED(buttons, BUTTON_LEFT_BUTTON)
#define BUTTON_RIGHT_PRESSED(buttons) BUTTON_PRESSED(buttons, BUTTON_RIGHT_BUTTON)

// RELEASED
#define BUTTON_A_RELEASED(buttons) BUTTON_RELEASED(buttons, BUTTON_A_BUTTON)
#define BUTTON_B_RELEASED(buttons) BUTTON_RELEASED(buttons, BUTTON_B_BUTTON)
#define BUTTON_START_RELEASED(buttons) BUTTON_RELEASED(buttons, BUTTON_START_BUTTON)
#define BUTTON_SELECT_RELEASED(buttons) BUTTON_RELEASED(buttons, BUTTON_SELECT_BUTTON)
#define BUTTON_UP_RELEASED(buttons) BUTTON_RELEASED(buttons, BUTTON_UP_BUTTON)
#define BUTTON_DOWN_RELEASED(buttons) BUTTON_RELEASED(buttons, BUTTON_DOWN_BUTTON)
#define BUTTON_LEFT_RELEASED(buttons) BUTTON_RELEASED(buttons, BUTTON_LEFT_BUTTON)
#define BUTTON_RIGHT_RELEASED(buttons) BUTTON_RELEASED(buttons, BUTTON_RIGHT_BUTTON)

// HELD
#define BUTTON_A_HELD(buttons) BUTTON_HELD(buttons, BUTTON_A_BUTTON)
#define BUTTON_B_HELD(buttons) BUTTON_HELD(buttons, BUTTON_B_BUTTON)
#define BUTTON_START_HELD(buttons) BUTTON_HELD(buttons, BUTTON_START_BUTTON)
#define BUTTON_SELECT_HELD(buttons) BUTTON_HELD(buttons, BUTTON_SELECT_BUTTON)
#define BUTTON_UP_HELD(buttons) BUTTON_HELD(buttons, BUTTON_UP_BUTTON)
#define BUTTON_DOWN_HELD(buttons) BUTTON_HELD(buttons, BUTTON_DOWN_BUTTON)
#define BUTTON_LEFT_HELD(buttons) BUTTON_HELD(buttons, BUTTON_LEFT_BUTTON)
#define BUTTON_RIGHT_HELD(buttons) BUTTON_HELD(buttons, BUTTON_RIGHT_BUTTON)

// UP
#define BUTTON_A_UP(buttons) BUTTON_UP(buttons[0], BUTTON_A_BUTTON)
#define BUTTON_B_UP(buttons) BUTTON_UP(buttons[0], BUTTON_B_BUTTON)
#define BUTTON_START_UP(buttons) BUTTON_UP(buttons[0], BUTTON_START_BUTTON)
#define BUTTON_SELECT_UP(buttons) BUTTON_UP(buttons[0], BUTTON_SELECT_BUTTON)
#define BUTTON_UP_UP(buttons) BUTTON_UP(buttons[0], BUTTON_UP_BUTTON)
#define BUTTON_DOWN_UP(buttons) BUTTON_UP(buttons[0], BUTTON_DOWN_BUTTON)
#define BUTTON_LEFT_UP(buttons) BUTTON_UP(buttons[0], BUTTON_LEFT_BUTTON)
#define BUTTON_RIGHT_UP(buttons) BUTTON_UP(buttons[0], BUTTON_RIGHT_BUTTON)

// DOWN
#define BUTTON_A_DOWN(buttons) BUTTON_DOWN(buttons[0], BUTTON_A_BUTTON)
#define BUTTON_B_DOWN(buttons) BUTTON_DOWN(buttons[0], BUTTON_B_BUTTON)
#define BUTTON_START_DOWN(buttons) BUTTON_DOWN(buttons[0], BUTTON_START_BUTTON)
#define BUTTON_SELECT_DOWN(buttons) BUTTON_DOWN(buttons[0], BUTTON_SELECT_BUTTON)
#define BUTTON_UP_DOWN(buttons) BUTTON_DOWN(buttons[0], BUTTON_UP_BUTTON)
#define BUTTON_DOWN_DOWN(buttons) BUTTON_DOWN(buttons[0], BUTTON_DOWN_BUTTON)
#define BUTTON_LEFT_DOWN(buttons) BUTTON_DOWN(buttons[0], BUTTON_LEFT_BUTTON)
#define BUTTON_RIGHT_DOWN(buttons) BUTTON_DOWN(buttons[0], BUTTON_RIGHT_BUTTON)

#endif
