extern unsigned char framecount;
#pragma zpsym ("framecount");
extern unsigned char totalframecount;
#pragma zpsym ("totalframecount");
extern unsigned int seconds;
#pragma zpsym ("seconds");
extern unsigned char second_tenths;
#pragma zpsym ("second_tenths");

extern void Nmi(void) {
    ++totalframecount;
    ++framecount;
    if (framecount >= 6) {
        framecount = 0;
        ++second_tenths;
        if (second_tenths >= 10) {
            second_tenths = 0;
            ++seconds;
        }
    }
}
