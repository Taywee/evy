; vim: ft=asm_ca65
.pushseg
.ZEROPAGE
n:
    .res 1
dest:
    .res 2
src:
    .res 2

.ifndef MEMCPY_ASM 
MEMCPY_ASM = 1
; y is the high byte of the number of bytes to copy, x is the low byte
; This is done because we don't know at this point whether the size is dynamic
; or not, so we let the user decide.
.macro memcpy dest_address, src_address
    .local loop, after
    sty n
    ldy #0
    lda #<dest_address
    sta dest
    lda #>dest_address
    sta dest + 1
    lda #<src_address
    sta src
    lda #>src_address
    sta src + 1
loop:
    lda (src), y
    sta (dest), y
    iny
    bne after
    ; y wrapped
    inc src + 1
    inc dest + 1
after:
    sec
    txa
    sbc #1
    tax
    lda n
    sbc #0
    sta n

    lda n
    bne loop
    txa
    bne loop
.endmacro
.endif
.popseg
