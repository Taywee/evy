#ifndef MOVEMENT_H
#define MOVEMENT_H

extern unsigned char movementstate;
#pragma zpsym ("movementstate");

#define MOVEMENT_STANDING 0x01
#define MOVEMENT_RUNNING 0x02
#define MOVEMENT_LEFT 0x04

#endif
