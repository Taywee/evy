#include "registers.h"
#include "sprites.h"
#include "movement.h"

struct Sprite {
    unsigned char y;
    unsigned char tile;
    unsigned char attributes;
    unsigned char x;
};

extern unsigned char playerpos[2];
#pragma zpsym ("playerpos");
extern unsigned char framecount;
#pragma zpsym ("framecount");
extern unsigned char second_tenths;
#pragma zpsym ("second_tenths");
extern unsigned int seconds;
#pragma zpsym ("seconds");

extern struct Sprite evy_sprites[4];

static unsigned char last_second_tenth = 0;
static unsigned char frame = 0;

extern void UpdateSprites(void) {
    evy_sprites[0].x = playerpos[0];
    evy_sprites[1].x = playerpos[0] + 8;
    evy_sprites[2].x = playerpos[0];
    evy_sprites[3].x = playerpos[0] + 8;
    evy_sprites[0].y = playerpos[1];
    evy_sprites[1].y = playerpos[1];
    evy_sprites[2].y = playerpos[1] + 8;
    evy_sprites[3].y = playerpos[1] + 8;

    // Mirroring
    if (movementstate & MOVEMENT_LEFT) {
        evy_sprites[0].attributes |= 0x40;
        evy_sprites[1].attributes |= 0x40;
        evy_sprites[2].attributes |= 0x40;
        evy_sprites[3].attributes |= 0x40;
        // Need to also reverse tile order
        evy_sprites[0].x += 8;
        evy_sprites[1].x -= 8;
        evy_sprites[2].x += 8;
        evy_sprites[3].x -= 8;
    } else {
        evy_sprites[0].attributes &= 0xBF;
        evy_sprites[1].attributes &= 0xBF;
        evy_sprites[2].attributes &= 0xBF;
        evy_sprites[3].attributes &= 0xBF;
    }
    if ((movementstate & (MOVEMENT_RUNNING | MOVEMENT_STANDING)) == (MOVEMENT_RUNNING | MOVEMENT_STANDING)) {
        if (second_tenths != last_second_tenth) {
            last_second_tenth = second_tenths;
            ++frame;
            if (frame >= 3) {
                frame = 0;
            }
            evy_sprites[0].tile = LEFT_PLAYER_0_0 + (frame * 4);
            evy_sprites[1].tile = LEFT_PLAYER_0_1 + (frame * 4);
            evy_sprites[2].tile = LEFT_PLAYER_0_2 + (frame * 4);
            evy_sprites[3].tile = LEFT_PLAYER_0_3 + (frame * 4);
        }
    } else {
        frame = 0;
        evy_sprites[0].tile = LEFT_PLAYER_0_0;
        evy_sprites[1].tile = LEFT_PLAYER_0_1;
        evy_sprites[2].tile = LEFT_PLAYER_0_2;
        evy_sprites[3].tile = LEFT_PLAYER_0_3;
    }
//    *PPUSCROLL = playerpos[0];
//    *PPUSCROLL = playerpos[1];
}
